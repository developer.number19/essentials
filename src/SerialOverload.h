#ifndef ESSENTIALS_ESSENTIAL_SERIAL_H
#define ESSENTIALS_ESSENTIAL_SERIAL_H

#include "Arduino.h"

#define endl "\r\n"

HardwareSerial & operator << (HardwareSerial & hs, const char * val) {
    hs.print(val);
    return hs;
}

HardwareSerial & operator << (HardwareSerial & hs, const String & val) {
    hs.print(val);
    return hs;
}

HardwareSerial & operator << (HardwareSerial & hs, int8_t val) {
    hs.print(val);
    return hs;
}

HardwareSerial & operator << (HardwareSerial & hs, uint8_t val) {
    hs.print(val);
    return hs;
}

HardwareSerial & operator << (HardwareSerial & hs, int16_t val) {
    hs.print(val);
    return hs;
}

HardwareSerial & operator << (HardwareSerial & hs, uint16_t val) {
    hs.print(val);
    return hs;
}

HardwareSerial & operator << (HardwareSerial & hs, int32_t val) {
    hs.print(val);
    return hs;
}

HardwareSerial & operator << (HardwareSerial & hs, uint32_t val) {
    hs.print(val);
    return hs;
}

HardwareSerial & operator << (HardwareSerial & hs, char val) {
    hs.print(val);
    return hs;
}

HardwareSerial & operator << (HardwareSerial & hs, float val) {
    hs.print(val);
    return hs;
}

bool isWhiteSpace(char c) {
    return c == '\r' || c == ' ' || c == '\n' || c == '\t';
}

void skipws(HardwareSerial & hs) {
    while (true) {
        if (hs.available()) {
            if (isWhiteSpace(Serial.peek())) {
                Serial.read();
            } else break;
        } else delay(1);
    }
}

HardwareSerial & operator >> (HardwareSerial & hs, String & val) {
    skipws(hs);
    while (true) {
        if (hs.available()) {
            char c = hs.peek();
            if (isWhiteSpace(hs.peek())) {
                break;
            } else val += (char) hs.read();
        } else delay(1);
    }
}

HardwareSerial & operator >> (HardwareSerial & hs, long & val) {
    bool first = true;
    bool negative = false;
    val = 0;
    skipws(hs);
    while (true) {
        if (hs.available()) {
            char c = Serial.peek(); // lol
            if ('0' <= c && c <= '9') {
                val *= 10;
                val += (int)(Serial.read() - '0'); // lol
            } else if (c == '-' || c == '+') {
                if (first) {
                    if (c == '-') {
                        negative = true;
                    }
                    Serial.read(); // lol
                } else {
                    break;
                }
            } else break;
            first = false;
        } else delay(1);
    }
    if (negative) val *= -1;
    return hs;
}

HardwareSerial & operator >> (HardwareSerial & hs, int & val) {
    bool first = true;
    bool negative = false;
    val = 0;
    skipws(hs);
    while (true) {
        if (hs.available()) {
            char c = Serial.peek(); // lol
            if ('0' <= c && c <= '9') {
                val *= 10;
                val += (int)(Serial.read() - '0'); // lol
            } else if (c == '-' || c == '+') {
                if (first) {
                    if (c == '-') {
                        negative = true;
                    }
                    Serial.read();
                } else {
                    break;
                }
            } else break;
            first = false;
        } else delay(1);
    }
    if (negative) val *= -1;
    return hs;
}

#endif