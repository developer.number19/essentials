#ifndef ESSENTIALS_MAP
#define ESSENTIALS_MAP

template<V>
class Map {
    static size_t hashCode(String key, size_t size) {
        size_t sum = 0;
        for (unsigned i = 0; i < key.length(); i++) {
            sum += (size_t) key[i]; 
        }
        return sum % size;
    }
    const size_t size;
    V ** table;
    public:
        Map(const size_t size = 16) : size(size), table(new V*[size]) {
            for (unsigned i = 0; i < size; i++) table[i] = NULL;
        }
        V & operator [] (const String key) {
            size_t i = hashCode(key, this->size);
            return *table[i];
        }
}



#endif