#ifndef ESSENTIALS_CONTAINER
#define ESSENTIALS_CONTAINER

template<typename T>
class Container {
    public:
        Container() : size(0) {}
        virtual bool isEmpty() const = 0;
        virtual size_t getSize() const = 0;
        virtual void add(T & items) = 0;
        virtual void add(const T & items) = 0;
        virtual void clear() = 0;
        //virtual void forEach(function<void(T & it)> fun) = 0;
        Container & operator << (T & item) {
            add(item);
            return *this;
        }
        Container & operator << (const T & item) {
            add(item);
            return *this;
        }
    protected:
        size_t size;
};


#endif