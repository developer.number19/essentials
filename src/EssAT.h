#ifndef ESSENTIALS_ESS_AT
#define ESSENTIALS_ESS_AT

#include <Arduino.h>

typedef String (*fun_s2s) (String);

String AT_FUN_UNDEFINED (String arg) {
    return "UNDEFINED";
}

struct SetGet {
    String key;
    fun_s2s set;
    fun_s2s get;
    SetGet * next;
};

class AT {
    const size_t size;
    SetGet ** funs;
    const String AT_PREFIX = "AT";
    String buffer = "";

    String process() {
        if (buffer == "AT") return "OK";
        if (buffer[AT_PREFIX.length()] != '+') return "Invalid character";
        buffer = buffer.substring(AT_PREFIX.length() + 1);
        char d = '=';
        int di = buffer.indexOf(d);
        if (di == -1) {
            d = '?';
            if ((di = buffer.indexOf(d)) == -1) return "Invalid operator";
        }
        String cmd = buffer.substring(0, di);
        size_t idx = hashFun(cmd);
        auto it = funs[idx];
        while (it) {
            if (it->key == cmd) {
                return d == '=' ? it->set(buffer.substring(di + 1)) : it->get(buffer.substring(di + 1));
            }
            it = it->next;
        }
        return "Unknown command";
    }
    size_t hashFun(String & key) {
        size_t sum = 0;
        for (size_t i = 0; i < key.length(); i++) sum += (int) key[i];
        return sum % size;
    }
    public:
        AT(const size_t size = 10) : size(size), funs(new SetGet*[size]) {
            for (size_t i = 0; i < size; i++) funs[i] = NULL;
        }
        void reg(String key, fun_s2s set, fun_s2s get) {
            size_t i = hashFun(key);
            SetGet * sg = new SetGet{key, set, get, NULL};
            if (!funs[i]) funs[i] = sg;
            else {
                auto it = funs[i];
                while (it->next) it = it->next;
                it->next = sg;
            }
        }
        String update(char c) {
            if (buffer.length() < AT_PREFIX.length()) {
                if (c == AT_PREFIX[buffer.length()]) buffer += c; else buffer = "";
            } else {
                if (c == '\r' || c == '\n') {
                    String msg = process();
                    buffer = "";
                    return msg;
                } else buffer += c;
            }
            return "";
        }
};


#endif