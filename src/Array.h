#ifndef ESSENTIALS_ARRAY
#define ESSENTIALS_ARRAY

#include "Container.h"

template<typename T>
class Array : public Container<T> {
    public:
        Array(size_t capacity) : capacity(capacity), items(new T[capacity]) {

        }
        T & operator [] (size_t index) {
            if (index < capacity) return items[index];
        }
        virtual bool isEmpty() const { return Container<T>::size == 0; }
        virtual size_t getSize() const { return Container<T>::size; }
        virtual void add(const T & item) {
            (*this)[Container<T>::size] = item;
        }
        virtual void add(T & item) {
            (*this)[Container<T>::size] = item;
        }
        virtual void clear() {
            delete [] items;
            items = new T[capacity];
            Container<T>::size = 0;
        }
        /*virtual void forEach(function<void(T & it)> fun) {
            for (size_t i = 0; i < size; i++) fun((*this)[i]);
        }*/
    private:
        T * items;
        const size_t capacity;
};

#endif