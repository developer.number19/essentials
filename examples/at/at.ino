#include <EssAT.h>

AT at;

String setRed(String arg) { return "RED" + arg; }
String getRed(String arg) { return "RED"; }
String setGreen(String arg) { return "GREEN" + arg; }
String getGreen(String arg) { return "GREEN"; }
String setBlue(String arg) { return "BLUE" + arg; }
String getBlue(String arg) { return "BLUE"; }
String setGray(String arg) { return "GRAY" + arg; }
String getGray(String arg) { return "GRAY"; }
String setYellow(String arg) { return "YELLOW" + arg; }
String getYellow(String arg) { return "YELLOW"; }
String setBlack(String arg) { return "BLACK" + arg; }
String getBlack(String arg) { return "BLACK"; }
String setPink(String arg) { return "PINK" + arg; }
String getPink(String arg) { return "PINK"; }
String setCyan(String arg) { return "CYAN" + arg; }
String getCyan(String arg) { return "CYAN"; }
String setWhite(String arg) { return "WHITE" + arg; }
String getWhite(String arg) { return "WHITE"; }

void setup() {
    Serial.begin(9600);
    while (!Serial);
    at.reg("RED", setRed, getRed);
    at.reg("GREEN", setGreen, getGreen);
    at.reg("BLUE", setBlue, getBlue);
    at.reg("GRAY", setGray, getGray);
    at.reg("YELLOW", setYellow, getYellow);
    at.reg("BLACK", setBlack, getBlack);
    at.reg("PINK", setPink, getPink);
    at.reg("CYAN", setCyan, getCyan);
    at.reg("WHITE", setWhite, getWhite);
}

void loop() {
    if (Serial.available()) {
        String msg = at.update((char) Serial.read());
        if (msg.length()) Serial.println(msg);
    }
}